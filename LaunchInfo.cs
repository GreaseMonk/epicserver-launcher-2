﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Launcher
{
    public struct LaunchInfo
    {
        public string   username,
                        password;

        public LaunchSettings launchSettings;
        public MCAuthenticationToken token;

        public bool     quickStart,
                        launchWithForge;

        public MainWindow caller;
    }
}
