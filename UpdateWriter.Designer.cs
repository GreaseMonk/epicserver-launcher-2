﻿namespace Launcher
{
    partial class UpdateWriter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitBtn = new System.Windows.Forms.Button();
            this.versionTxtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.downloadURLTxtBox = new System.Windows.Forms.TextBox();
            this.generateJsonBtn = new System.Windows.Forms.Button();
            this.deleteSettingsCheckbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(424, 12);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(45, 23);
            this.exitBtn.TabIndex = 7;
            this.exitBtn.Text = "X";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // versionTxtBox
            // 
            this.versionTxtBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.versionTxtBox.Location = new System.Drawing.Point(98, 49);
            this.versionTxtBox.Name = "versionTxtBox";
            this.versionTxtBox.Size = new System.Drawing.Size(81, 20);
            this.versionTxtBox.TabIndex = 8;
            this.versionTxtBox.Text = "1234";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Version Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Download URL";
            // 
            // downloadURLTxtBox
            // 
            this.downloadURLTxtBox.Location = new System.Drawing.Point(98, 79);
            this.downloadURLTxtBox.Name = "downloadURLTxtBox";
            this.downloadURLTxtBox.Size = new System.Drawing.Size(371, 20);
            this.downloadURLTxtBox.TabIndex = 10;
            this.downloadURLTxtBox.Text = "http://";
            // 
            // generateJsonBtn
            // 
            this.generateJsonBtn.Location = new System.Drawing.Point(319, 138);
            this.generateJsonBtn.Name = "generateJsonBtn";
            this.generateJsonBtn.Size = new System.Drawing.Size(150, 23);
            this.generateJsonBtn.TabIndex = 12;
            this.generateJsonBtn.Text = "Generate JSON";
            this.generateJsonBtn.UseVisualStyleBackColor = true;
            this.generateJsonBtn.Click += new System.EventHandler(this.generateJsonBtn_Click);
            // 
            // deleteSettingsCheckbox
            // 
            this.deleteSettingsCheckbox.AutoSize = true;
            this.deleteSettingsCheckbox.Location = new System.Drawing.Point(98, 105);
            this.deleteSettingsCheckbox.Name = "deleteSettingsCheckbox";
            this.deleteSettingsCheckbox.Size = new System.Drawing.Size(96, 17);
            this.deleteSettingsCheckbox.TabIndex = 13;
            this.deleteSettingsCheckbox.Text = "Delete settings";
            this.deleteSettingsCheckbox.UseVisualStyleBackColor = true;
            // 
            // UpdateWriter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 170);
            this.Controls.Add(this.deleteSettingsCheckbox);
            this.Controls.Add(this.generateJsonBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.downloadURLTxtBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.versionTxtBox);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UpdateWriter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UpdateWriter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.TextBox versionTxtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox downloadURLTxtBox;
        private System.Windows.Forms.Button generateJsonBtn;
        private System.Windows.Forms.CheckBox deleteSettingsCheckbox;
    }
}