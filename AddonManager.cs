﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Launcher
{
    public class AddonManager
    {
        private MainWindow mainWindow = null;
        private CheckedListBox listBox;

        public AddonManager(MainWindow windowRef)
        {
            mainWindow = windowRef;
            listBox = mainWindow.addonsListBox;
        }

        public void FillAddonList()
        {
            DirectoryInfo dirInfos = new DirectoryInfo(MainWindow.minecraftDir + @"\mods\");
            if (Directory.Exists(dirInfos.FullName))
            {
                FileSystemInfo[] fileSysInfos = dirInfos.GetFileSystemInfos("*.jar", SearchOption.TopDirectoryOnly);
                listBox.Items.AddRange(fileSysInfos);
            }
        }
    }
}
