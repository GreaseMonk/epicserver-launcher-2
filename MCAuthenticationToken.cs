﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Launcher
{
    public class MCAuthenticationToken
    {
        public struct Profile
        {
            public string id;           // hexadecimal
            public string name;         // the player name
        }
        public struct User
        {
            public struct Property
            {
                public string name;     // might not be present for all accounts
                public string value;    // ie. 'en' - ISO 639-1?
            }
            public string id;           // hexadecimal
            public Property properties;
        }

        public string accessToken;
        public string clientToken;
        public Profile selectedProfile;
        public Profile[] availableProfiles;
        public User user;
        public string error;
        public string errorMessage;

        /// <summary>
        /// Parse a json string to the MCAuthenticationToken class
        /// </summary>
        /// <param name="TargetFile">Path to the file (any extension, but usually .json or .txt)</param>
        /// <returns>The MCAuthenticationToken object that reflects the targeted json file.</returns>
        public static MCAuthenticationToken parse(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<MCAuthenticationToken>(jsonString);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't parse Json: ");
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}
