﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Launcher
{
    // http://s3.amazonaws.com/Minecraft.Download/versions/versions.json
    public class VersionsIdentifier
    {
        public struct Latest
        {
            public string snapshot;
            public string release;
        }
        public struct Versions
        {
            public string id;
            public string time;
            public string releaseTime;
            public string type;
        }

        public Latest latest;
        public Versions[] versions;

        /// <summary>
        /// Turn a .json file from minecraft to a format which can be easily iterated.
        /// </summary>
        /// <param name="TargetFile">Path to the file (any extension, but usually .json or .txt)</param>
        /// <returns>The MCIdentifier object that reflects the targeted json file.</returns>
        public static VersionsIdentifier Identify(string targetString)
        {
            VersionsIdentifier versionsIdentifier = null;
            versionsIdentifier = JsonConvert.DeserializeObject<VersionsIdentifier>(targetString);
            return versionsIdentifier;
        }
    }
}
