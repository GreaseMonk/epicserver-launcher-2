﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using System.Windows.Forms;

namespace Launcher
{
    public static class ArchiveHandler
    {
        /// <summary>
        /// Author: "salysle" from codeproject.com
        /// Source: http://www.codeproject.com/Articles/27606/Opening-Jars-with-C
        /// 
        /// Used under the terms of The Code Project Open License (CPOL).
        /// http://www.codeproject.com/info/cpol10.aspx
        /// 
        /// Extracts the jar file to a specified
        /// destination folder.  
        /// </summary>
        public static void ExtractJar(string pathToJar, string saveFolderPath)
        {
            string jarPath = pathToJar;
            string savePath = saveFolderPath;

            try
            {
                // verify the paths are set
                if (!String.IsNullOrEmpty(jarPath) &&
                   !String.IsNullOrEmpty(saveFolderPath))
                {
                    try
                    {
                        // use the SharpZip library FastZip
                        // utilities ExtractZip method to 
                        // extract the jar file
                        FastZip fz = new FastZip();
                        fz.ExtractZip(jarPath, saveFolderPath, "");
                    }
                    catch (Exception ex)
                    {
                        // something went wrong
                        MessageBox.Show(ex.Message, "Extraction Error");
                    }
                }
                else
                {
                    // the paths were not, tell the user to 
                    // get with the program

                    StringBuilder sb = new StringBuilder();
                    sb.Append("Set the paths to both the jar file and " + Environment.NewLine);
                    sb.Append("destination folder before attempting to " + Environment.NewLine);
                    sb.Append("to extract a jar file.");

                    MessageBox.Show(sb.ToString(), "Unable to Extract");
                }
            }
            catch (Exception ex)
            {
                // something else went wrong
                MessageBox.Show(ex.Message, "Extraction Method Error");
            }
        }

    }
}
