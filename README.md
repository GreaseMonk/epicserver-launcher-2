---
# EpicServer.nl launcher 2!
#### A nice base to create your own C# launcher in Visual Studio for your server's players.
---

#### The current state of this launcher

It needs pull requests so that authentication works, and downloading the minecraft base game works. There is a beginning for both of them that have great potential and may not take alot of time to fix.

Pros:

+ Able to connect to a server without seeing a main menu
+ A button for the live map, your players do not need to bookmark it
+ JVM options can be manually inserted and comes with examples
+ Auto-updater

Cons:

+ Minecraft's login system changes every season, and the authentication within the launcher must be rewritten.
+ Requires some work to get it going for your needs
+ Due to copyrights, I can't really download minecraft itself, and thats why it's not updated here yet. (I will contact Mojang again soon but I think if they grant permission for me, it won't mean that right is passed on to everyone who forks this repository.)
+ It's much simpler and more straight forward than launchers such as FTB or Curse or Technic, but this might be a pro