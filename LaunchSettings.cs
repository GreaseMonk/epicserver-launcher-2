﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;

namespace Launcher
{
    /// <summary>
    /// A class created to achieve persistent data for this program.
    /// The file is stored in the .minecraft folder called LaunchSettings.json
    /// It can be modified manually!
    /// </summary>
    public class LaunchSettings
    {
        public struct LauncherSettings
        {
            public string minecraftVersion;
            public string forgeVersion;
            public string nativesVersion;
            public string jvmArguments;
        }

        public bool debugMode;
        public bool closeWhenStarting;
        public bool autoCheckUpdates;
        public bool launchWithForge;
        public bool ignoreAlreadyRunningMsg;
        public string quickStartUrl;
        public string mapServerUrl;
        public int version;
        public LauncherSettings launcherSettings;

        /// <summary>
        /// Reads a JSON file that has the LaunchSettings structure.
        /// </summary>
        /// <param name="TargetFile">Path to the JSON file.</param>
        /// <returns>The deserialized values</returns>
        public static LaunchSettings Read(string TargetFile)
        {
            if (File.Exists(TargetFile))
            {
                LaunchSettings settings = null;
                try
                {
                    StreamReader reader = new StreamReader(TargetFile);
                    settings = JsonConvert.DeserializeObject<LaunchSettings>(reader.ReadToEnd());
                    reader.Close();
                    return settings;
                }
                catch (Exception ex)
                {
                    string title = "Read Error";
                    string message = "Couldn't parse Settings Json File: \n " + ex.Message +
                        "\n It could be that the structure has changed by the author \n" +
                        "Delete this file and generate a new one?";
                    DialogResult result = MessageBox.Show(message, title, MessageBoxButtons.YesNo);

                    switch (result)
                    {
                        case DialogResult.Yes:
                            File.Delete(TargetFile);
                            break;
                        case DialogResult.No:
                            return null;
                        default: // In case the user presses the X button.
                            return null;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Create a LaunchSettings structure.
        /// </summary>
        /// <param name="identifier">Creates a JSON file in LaunchSettings structure.</param>
        /// <param name="TargetFile">for example @"C:\LaunchSettings.json"</param>
        /// <returns>True if the operation succeeded.</returns>
        public static bool Create(LaunchSettings settings, string TargetFile)
        {
            JsonSerializerSettings jsonSettings = new JsonSerializerSettings
            {
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Full,
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
            string json = JsonConvert.SerializeObject(settings, Formatting.Indented);

        RetryCreation:
            try
            {
                string settingsFolderPath = TargetFile.Substring(0, TargetFile.Length - MainWindow.settingsFileName.Length);
                if(!Directory.Exists(settingsFolderPath))
                {
                    Directory.CreateDirectory(settingsFolderPath);
                }

                System.IO.StreamWriter file = new System.IO.StreamWriter(TargetFile);
                file.WriteLine(json);
                file.Close();
            }
            catch (System.IO.IOException ex)
            {
                if (MessageBox.Show("File is in use. \n" + ex.Message, "Apparently...", MessageBoxButtons.RetryCancel) == DialogResult.Retry)
                    goto RetryCreation;
            }
            return true;
        }

        /// <summary>
        /// Gets a default setting to create a file with.
        /// </summary>
        /// <returns>A default LaunchSettings class</returns>
        public static LaunchSettings GetDefault()
        {
            LaunchSettings defaultSettings = new LaunchSettings();
            LaunchSettings.LauncherSettings defaultLauncherSettings = new LaunchSettings.LauncherSettings();

            defaultSettings.debugMode = false;
            defaultSettings.closeWhenStarting = true;
            defaultSettings.launchWithForge = false;
            defaultSettings.quickStartUrl = "epicserver.nl";
            defaultSettings.mapServerUrl = "http://www.minecraft.net/";
            defaultSettings.ignoreAlreadyRunningMsg = false;
            defaultSettings.autoCheckUpdates = true;

            defaultLauncherSettings.minecraftVersion = "1.8.jar";
            defaultLauncherSettings.forgeVersion = "1.6.2-Forge9.10.0.841.jar";
            defaultLauncherSettings.nativesVersion = "lwjgl-platform-2.9.1-natives-windows.jar";

            defaultSettings.version = 0;
            defaultSettings.launcherSettings = defaultLauncherSettings;
            return defaultSettings;
        }
    }
}
