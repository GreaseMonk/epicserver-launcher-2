﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Launcher
{
    public partial class MainWindow : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public AddonManager addonMgr;
        public static string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        public static string minecraftDir = appData + @"\.minecraft";
        public static string minecraftVersionsDir = minecraftDir + @"\versions";
        public static string settingsFileName = "GCSettings.json";

        public static string[] nativesFileNames = new string[4] { "lwjgl.dll", "lwjgl64.dll", "OpenAL32.dll", "OpenAL64.dll" };
        public static string nativesDir = minecraftDir + @"\libraries\org\lwjgl\lwjgl\lwjgl-platform";
        public static string settingsPath = appData + @"\.minecraft\" + settingsFileName;
        public LaunchSettings settings;

        private LauncherUpdater updater;

        public int version;

        public bool IsMinecraftInstalled
        {
            get
            {
                if (settings == null)
                    GenerateNewSettingsFile();

                string globalVersion = string.Empty;

                if(settings.launcherSettings.minecraftVersion.Length >= 5)
                    globalVersion = settings.launcherSettings.minecraftVersion.Substring(0, 5);

                string versionDir = String.Format(@"{0}\{1}\", minecraftVersionsDir, globalVersion);
                string versionJar = String.Format(@"{0}\{2}\{1}", minecraftVersionsDir, settings.launcherSettings.minecraftVersion, globalVersion);

                bool mainDirectory = Directory.Exists(minecraftDir);
                bool nativesDirectory = Directory.Exists(nativesDir);
                bool versionDirectory = Directory.Exists(versionDir);
                bool versionJarPath = File.Exists(versionJar);

                AddConsoleLine("(INIT) Detecting minecraft installation...");
                AddConsoleLine("(INIT) LauncherSettings set to version " + globalVersion);
                AddConsoleLine(String.Format("Main Dir:     {0}", mainDirectory));
                AddConsoleLine(String.Format("Natives Dir:  {0}", nativesDirectory));
                AddConsoleLine(String.Format("Versions Dir: {0}", versionDirectory));
                AddConsoleLine(String.Format("Jar file:     {0}", versionJarPath));

                if (mainDirectory && nativesDirectory && versionDirectory && versionJarPath)
                    return true;

                return false;
            }
        }

        public MainWindow()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            // This fixes the TabControl flickering on mouse over.
            // It happens because transparency is rendered before the tab, and we have a background.
            //Application.VisualStyleState = System.Windows.Forms.VisualStyles.VisualStyleState.NoneEnabled;

            InitializeComponent();
            this.KeyPreview = true;

            if (System.AppDomain.CurrentDomain.FriendlyName.Contains("_new"))
                ExecuteCurrentUpdate();

            // Check for and delete temporary update files.
            DeleteTempFiles();
        }
        

        /// <summary>
        /// Executed on load of MainWindow.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Load(object sender, EventArgs e)
        {
            // This is a newly installed version, don't start just yet.
            if (System.AppDomain.CurrentDomain.FriendlyName.Contains("_new"))
                return;

            // Clear the console.
            consoleBox.Text = String.Empty;
            AddConsoleLine("//");
            AddConsoleLine("//     Author: Wiebe Geertsma, www.linkedin.com/profile/view?id=124424700");
            AddConsoleLine("//     GCMCLauncher Alpha");
            AddConsoleLine("//");
            AddConsoleLine("Initializing GUI components completed");

            // Make sure to only load when minecraft is installed.
            if (IsMinecraftInstalled)
                DoStartUpCheck();
            else
            {
                string boxText = "Minecraft was not detected. Install Minecraft now ? Press 'No' to close the application.";
                string boxCaption = "Minecraft Installation";

                if (MessageBox.Show(boxText, boxCaption, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // Don't show the main window since settings will be null.
                    this.WindowState = FormWindowState.Minimized;
                    this.ShowInTaskbar = false;
                    new VersionInstallWindow(this);
                }
                else
                    Application.Exit();
            }
            
        }

        /// <summary>
        /// Check if any instance of java is currently running.
        /// </summary>
        /// <returns></returns>
        public static bool IsJavaRunning
        {
            get
            {
                System.Diagnostics.Process[] pname = System.Diagnostics.Process.GetProcessesByName("java");
                if (pname.Length != 0)
                    return true;
                return false;
            }
        }

        #region Form Events


        /// <summary>
        /// Close the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitBtn_Click(object sender, EventArgs e)
        {
            AddConsoleLine("Saving Preferences...");
            SaveSettingsFile();
            AddConsoleLine("Exiting Application...");
            Application.Exit();
        }

        /// <summary>
        /// Minimize the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void minimizeBtn_Click(object sender, EventArgs e)
        {
            AddConsoleLine("Minimizing Window");
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// Executed whenever the user presses any key when
        /// the main window is active.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                AddConsoleLine("Exiting Application...");
                Application.Exit();
            }
        }

        /// <summary>
        /// Starts minecraft normally.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startBtn_Click(object sender, EventArgs e)
        {
            AddConsoleLine("Assembling LaunchInfo.");
            SaveSettingsFile();

            // We assemble the initial launch info.
            LaunchInfo launchInfo = new LaunchInfo();
            launchInfo.clientVersion = "13";
            launchInfo.username = usernameBtn.Text;
            launchInfo.password = passwordLabel.Text;
            launchInfo.launchSettings = settings;
            launchInfo.quickStart = true;
            launchInfo.launchSettings.closeWhenStarting = closeWhenMCStartsCheckbox.Checked;
            launchInfo.launchWithForge = launchWithForgeCheckbox.Checked;
            launchInfo.caller = this;


            if (String.IsNullOrEmpty(launchInfo.password) || String.IsNullOrEmpty(launchInfo.username) || launchInfo.username == "Click here or press Start to login." || launchInfo.username == "user@used.com")
            {
                AddConsoleLine("Login credentials required.");
                LoginWindow loginWindow = new LoginWindow(this, launchInfo);
                loginWindow.Show();
            }
            else
            {
                AddConsoleLine("Using persistent login credentials to login...");
                GenerateSession(launchInfo);
            }
            
        }

        /// <summary>
        /// Displays the username, but can also be clicked
        /// to change the currently logged in user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void usernameBtn_Click(object sender, EventArgs e)
        {
            // We assemble the initial launch info.

            LaunchInfo launchInfo = new LaunchInfo();
            launchInfo.clientVersion = "13";
            launchInfo.username = usernameBtn.Text;
            launchInfo.password = passwordLabel.Text;
            launchInfo.launchSettings = settings;
            launchInfo.quickStart = true;
            launchInfo.launchSettings.closeWhenStarting = closeWhenMCStartsCheckbox.Checked;
            launchInfo.launchWithForge = launchWithForgeCheckbox.Checked;
            launchInfo.caller = this;

            LoginWindow loginWindow = new LoginWindow(this, launchInfo, false);
            loginWindow.Show();
        }

        /// <summary>
        /// Executed on mouse down on the main form window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                AddConsoleLine("Showing UpdateWriter");
                new UpdateWriter(this).Show();
            }
            if(e.KeyCode == Keys.F3)
            {
                new VersionInstallWindow(this);
            }
        }

        private void autoCheckUpdatesCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            settings.autoCheckUpdates = autoCheckUpdatesCheckbox.Checked;
            SaveSettingsFile();
        }

        private void closeWhenMCStartsCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            settings.closeWhenStarting = closeWhenMCStartsCheckbox.Checked;
            SaveSettingsFile();
        }

        #endregion

        /// <summary>
        /// This is a process that renames the current file.
        /// It is executed during an update.
        /// </summary>
        public void ExecuteCurrentUpdate()
        {
            AddConsoleLine("(Updater) This is a new launcher. Starting a renaming process.");

            if (File.Exists("GCMCLauncher.exe"))
            {
                AddConsoleLine("(Updater) Deleting existing GCMCLauncher.exe file.");
                File.Delete(Application.StartupPath + @"/GCMCLauncher.exe");
            }

            AddConsoleLine("(Updater) Copying this new launcher as GCMCLauncher.exe.");
            File.Copy(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName, "GCMCLauncher.exe");

            Process.Start(Application.StartupPath + @"/GCMCLauncher.exe");
            try
            {
                Environment.Exit(-1);
            }
            catch (System.Security.SecurityException ex)
            {
                MessageBox.Show("Because of your security features, my program was unable to process a couple of things." +
                                "You should delete the _new file and open GCMCLauncher.exe. (" + ex.Message + ")");
            }
        }

        /// <summary>
        /// Execute all functions that should be
        /// done when minecraft is installed.
        /// This is called on load or if a minecraft
        /// installation has just finished.
        /// </summary>
        public void DoStartUpCheck()
        {
            LoadLauncherSettings();
            CheckJavaRunning();
            GetLoginCredentials();

            version = Convert.ToInt32(Application.ProductVersion.Split('.')[0]);
            versionLabel.Text = "Version No. " + version.ToString();

            // Check for updates in the background.
            if (settings.autoCheckUpdates)
                updater = new LauncherUpdater(this);

            // Initialize the addon tab.
            // Work in progress.
            addonMgr = new AddonManager(this);
            addonMgr.FillAddonList();
            AddConsoleLine("Filling addon list");
        }

        /// <summary>
        /// Check for and delete temporary files from
        /// the previous update.
        /// </summary>
        public void DeleteTempFiles()
        {
            string[] oldLaunchers = Directory.GetFiles(Application.StartupPath, "*_new.exe", SearchOption.TopDirectoryOnly);
            if (oldLaunchers.Length > 0)
            {
                AddConsoleLine("(Updater) Deleting temporary files from the last update.");
                foreach (string fileName in oldLaunchers)
                {
                    try
                    {
                        File.Delete(fileName);
                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine("Unable to delete " + fileName + ": " + ex.Message);
                    }

                }
            }
        }

        /// <summary>
        /// Loads all previous user settings such as
        /// checkboxes in the settings tab.
        /// </summary>
        public void LoadLauncherSettings()
        {
            if (File.Exists(settingsPath))
            {
                settings = LaunchSettings.Read(settingsPath);
                closeWhenMCStartsCheckbox.Checked = settings.closeWhenStarting;
                autoCheckUpdatesCheckbox.Checked = settings.autoCheckUpdates;
                launchWithForgeCheckbox.Checked = settings.launchWithForge;
            }
            else
            {
                settings = LaunchSettings.GetDefault();
                LaunchSettings.Create(settings, settingsPath);
            }
        }

        /// <summary>
        /// Warn the user if a java process is already
        /// running in the background. Sometimes
        /// this could be useful.
        /// </summary>
        public void CheckJavaRunning()
        {
        RetryCheckJava:
            if (IsJavaRunning && !settings.ignoreAlreadyRunningMsg)
            {
                var message = "An instance of Java is already running (Minecraft?) \n This COULD cause issues.";
                var title = "Continue?";
                var result = MessageBox.Show(message, title, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Question);

                switch (result)
                {
                    case DialogResult.Abort:
                        Application.Exit();
                        break;
                    case DialogResult.Retry:
                        goto RetryCheckJava;
                    case DialogResult.Ignore:
                        if (MessageBox.Show("Ignore 'Already Running' message in the future?", "Notification", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            settings.ignoreAlreadyRunningMsg = true;
                            LaunchSettings.Create(settings, settingsPath);
                        }
                        break;
                    default:
                        // X button was pressed. Abort then anyway.
                        Application.Exit();
                        break;
                }
            }
        }

        /// <summary>
        /// Gets the stored login credentials to
        /// show the current login profile in the
        /// interface.
        /// </summary>
        public void GetLoginCredentials()
        {
            if (System.IO.File.Exists(appData + @"\.minecraft\EpicLoginProtected.txt"))
            {
                string storedCredentials;

                try
                {
                    DecryptFile(appData + @"\.minecraft\EpicLoginProtected.txt", appData + @"\.minecraft\EpicLogin.txt", GetKey());

                }
                catch (Exception ex)
                {
                    System.IO.File.Delete(appData + @"\.minecraft\EpicLoginProtected.txt");
                    MessageBox.Show("This current EpicLogin file is not valid for this machine. \n A new one is generated the next time you successfully login. \n" + ex.Message);
                }
                if (System.IO.File.Exists(appData + @"\.minecraft\EpicLogin.txt"))
                {

                    storedCredentials = System.IO.File.ReadAllText(appData + @"\.minecraft\EpicLogin.txt");
                    System.IO.File.Delete(appData + @"\.minecraft\EpicLogin.txt");

                    string[] epicLoginCreds = storedCredentials.Split('&');
                    string[] strByteUser = epicLoginCreds[0].Split(':');
                    string[] strBytePass = epicLoginCreds[1].Split(':');

                    byte[] byteUser = new byte[strByteUser.Length];
                    byte[] bytePass = new byte[strBytePass.Length];

                    for (int i = 0; i < strByteUser.Length; i++)
                        byteUser[i] = Byte.Parse(strByteUser[i]);
                    for (int i = 0; i < strBytePass.Length; i++)
                        bytePass[i] = Byte.Parse(strBytePass[i]);

                    string storedUser = System.Text.Encoding.UTF8.GetString(byteUser);
                    string storedPass = System.Text.Encoding.UTF8.GetString(bytePass);

                    usernameBtn.Text = storedUser.ToString();
                    passwordLabel.Text = storedPass.ToString();
                }
            }
            if (usernameBtn.Text == String.Empty || usernameBtn.Text == "user@used.com") // Test label for demo screenshot.
            {
                usernameBtn.Text = "Click here or press Start to login.";
            }
        }

        /// <summary>
        /// Adds a line to the console.
        /// </summary>
        /// <param name="line">The line to add.</param>
        public void AddConsoleLine(string line)
        {
            string currentContent = consoleBox.Text;
            string newContent = String.Empty;
            string prefix = "[" + DateTime.Now.ToString("HH:mm:ss") + "]: ";

            // Also write the line to the debug console
            // for debugging.
            Console.WriteLine(prefix + line);


            /* Top to bottom writing
            newContent += prefix;
            newContent += line;
            newContent += Environment.NewLine;
            newContent += currentContent;
            */

            //newContent += currentContent;
            newContent += Environment.NewLine;
            newContent += prefix;
            newContent += line;

            consoleBox.AppendText(newContent);
        }

        /// <summary>
        /// Completes the login with the given launchInfo.
        /// </summary>
        /// <param name="launchInfo"></param>
        public void CompleteLogin(LaunchInfo launchInfo, bool autoStart)
        {
            // Update the username with the one that has just been
            // filled into the login window.
            usernameBtn.Text = launchInfo.username.ToString();

            if (autoStart)
                GenerateSession(launchInfo);
        }

        /// <summary>
        /// Used to generate a session with login.minecraft.net for online use only
        /// </summary>
        /// <param name="username">The player's username</param>
        /// <param name="password">The player's password</param>
        /// <param name="clientVer">The client version (look here http://wiki.vg/Session)</param>
        /// <returns></returns>
        private void GenerateSession(LaunchInfo launchInfo)
        {
            string requestURL = string.Format(
                "https://authserver.mojang.com?user={0}&password={1}&version={2}",
                launchInfo.username,
                launchInfo.password,
                launchInfo.clientVersion);
            requestURL.Split(':');

            // Make a web request and do it in a seperate thread
            // to prevent blocking the Interface.
            WebRequest request = WebRequest.Create(requestURL);
            Task<WebResponse> waitingResponse = AsyncResponder.GetResponseAsync(request, new CancellationToken());

            // Wait until the request comes back with a response.
            waitingResponse.Wait();
            WebResponse response = waitingResponse.Result;
            StreamReader responseStream = new StreamReader(response.GetResponseStream());

            string stringResponse = responseStream.ReadToEnd();
            stringResponse.Trim();

            // Returns 5 values split by ":" Current Version : Download Ticket : Username : Session ID : UID.
            // http://wiki.vg/Legacy_Authentication
            // For information.
            string[] mcSession = stringResponse.Split(':');

            if (mcSession[0] == "Bad login")
                AddConsoleLine("Unable to Login: " + mcSession[0] + ".");
            else
            {
                AddConsoleLine("Successfully logged in as " + mcSession[2] + ".");

                // Write information to the container.
                launchInfo.character = mcSession[2];
                launchInfo.sessionID = mcSession[3];
                launchInfo.UID = mcSession[4];

                AddConsoleLine("Encrypting & Saving credentials.");
                SaveCredentials(launchInfo.username, launchInfo.password);

                // The batch assembler will make the entire string
                // command, and will eventually launch minecraft.
                new BatchAssembler(this, launchInfo);
            }
        }

        /// <summary>
        /// Save the username and password using encryption
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public void SaveCredentials(string user, string password)
        {
            byte[] storedUser = Encoding.UTF8.GetBytes(user);
            byte[] storedPass = Encoding.UTF8.GetBytes(password);
            string[] storedData = new String[2];
            foreach (byte b in storedUser)
                storedData[0] += b.ToString() + ":";
            foreach (byte b in storedPass)
                storedData[1] += b.ToString() + ":";

            storedData[0] = storedData[0].Remove(storedData[0].Length - 1);
            storedData[1] = storedData[1].Remove(storedData[1].Length - 1);

            string epicLoginCreds = storedData[0] + "&" + storedData[1];

            if (File.Exists(appData + @"\.minecraft\EpicLogin.txt"))
                File.Delete(appData + @"\.minecraft\EpicLogin.txt");
            System.IO.File.WriteAllText(appData + @"\.minecraft\EpicLogin.txt", epicLoginCreds);
            FileInfo protectionFile = new FileInfo(appData + @"\.minecraft\EpicLoginProtected.txt");
            if (File.Exists(appData + @"\.minecraft\EpicLoginProtected.txt"))
            {
                for (int attempts = 0; attempts <= 3 && IsFileLocked(protectionFile); attempts++)
                {
                    try
                    {
                        System.IO.File.Delete(appData + @"\.minecraft\EpicLoginProtected.txt");
                    }
                    catch (System.Exception ex)
                    {
                        AddConsoleLine(ex.Message);
                    }
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(0.3));
                    if (attempts == 3)
                    {
                        AddConsoleLine("Couldn't access EpicLoginProtected after 3 tries.");
                        return;
                    }
                }
            }
            EncryptFile(appData + @"\.minecraft\EpicLogin.txt", appData + @"\.minecraft\EpicLoginProtected.txt", GetKey());
            System.IO.File.Delete(appData + @"\.minecraft\EpicLogin.txt");
        }

        /// <summary>
        /// Encrypts the lock file.
        /// </summary>
        /// <param name="sInputFilename"></param>
        /// <param name="sOutputFilename"></param>
        /// <param name="sKey"></param>
        public static void EncryptFile(string sInputFilename, string sOutputFilename, string sKey)
        {
            FileStream fsInput = new FileStream(sInputFilename,
               FileMode.Open,
               FileAccess.Read);

            FileStream fsEncrypted = new FileStream(sOutputFilename,
               FileMode.Create,
               FileAccess.Write);
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
            DES.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            DES.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
            ICryptoTransform desencrypt = DES.CreateEncryptor();
            CryptoStream cryptostream = new CryptoStream(fsEncrypted,
               desencrypt,
               CryptoStreamMode.Write);

            byte[] bytearrayinput = new byte[fsInput.Length];
            fsInput.Read(bytearrayinput, 0, bytearrayinput.Length);
            cryptostream.Write(bytearrayinput, 0, bytearrayinput.Length);
            cryptostream.Close();
            fsInput.Close();
            fsEncrypted.Close();
        }

        /// <summary>
        /// Decrypts the lock file.
        /// </summary>
        /// <param name="sInputFilename"></param>
        /// <param name="sOutputFilename"></param>
        /// <param name="sKey"></param>
        public static void DecryptFile(string sInputFilename, string sOutputFilename, string sKey)
        {
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
            //A 64 bit key and IV is required for this provider.
            //Set secret key For DES algorithm.
            DES.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            //Set initialization vector.
            DES.IV = ASCIIEncoding.ASCII.GetBytes(sKey);

            //Create a file stream to read the encrypted file back.
            FileStream fsread = new FileStream(sInputFilename,
                                           FileMode.Open,
                                           FileAccess.Read);
            //Create a DES decryptor from the DES instance.
            ICryptoTransform desdecrypt = DES.CreateDecryptor();
            //Create crypto stream set to read and do a 
            //DES decryption transform on incoming bytes.
            CryptoStream cryptostreamDecr = new CryptoStream(fsread,
                                                         desdecrypt,
                                                         CryptoStreamMode.Read);
            //Print the contents of the decrypted file.
            StreamWriter fsDecrypted = new StreamWriter(sOutputFilename);
            fsDecrypted.Write(new StreamReader(cryptostreamDecr).ReadToEnd());
            fsDecrypted.Flush();
            fsDecrypted.Close();
            fsread.Close();
        }

        /// <summary>
        /// Get a system-specific key.
        /// </summary>
        /// <returns></returns>
        private static string GetKey()
        {
            string key = String.Empty;

            int repeat = 0;

            for (int i = 0; i < 5; i++, repeat++)
            {
                if (repeat > System.Environment.MachineName.Length)
                    repeat = 0;

                key += System.Environment.MachineName[repeat];
            }

            key = key.Substring(0, 5);
            key += "???";
            return key;
        }

        /// <summary>
        /// Check if the given file is locked, being written to.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        /// <summary>
        /// Get a specific jar library by file name.
        /// </summary>
        /// <param name="targetFileName"></param>
        /// <returns></returns>
        public static FileInfo GetLibrary(string targetFileName)
        {
            FileInfo retVal = null;
            DirectoryInfo dir = new DirectoryInfo(appData + @"\.minecraft\libraries");

            foreach (FileInfo file in dir.GetFiles("*.jar", SearchOption.AllDirectories))
            {
                if (file.Name == targetFileName)
                    return file;
                else if (file.Name == targetFileName.Substring(0, targetFileName.Length - 4) + "-natives-windows.jar")
                    return file;
            }
            return retVal;
        }

        /// <summary>
        /// Check for updates.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkForUpdatesBtn_Click(object sender, EventArgs e)
        {
            if (updater == null)
                updater = new LauncherUpdater(this);
            else
                updater.CheckForUpdates(true);
        }

        /// <summary>
        /// Occurs when the resolution of an assembly fails.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string dllName = args.Name.Contains(',') ? args.Name.Substring(0, args.Name.IndexOf(',')) : args.Name.Replace(".dll", "");

            dllName = dllName.Replace(".", "_");

            if (dllName.EndsWith("_resources")) return null;

            System.Resources.ResourceManager rm = new System.Resources.ResourceManager(GetType().Namespace + ".Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());

            byte[] bytes = (byte[])rm.GetObject(dllName);

            return System.Reflection.Assembly.Load(bytes);
        }

        public void SaveSettingsFile()
        {
            LaunchSettings.Create(settings, settingsPath);
        }

        public void GenerateNewSettingsFile()
        {
            settings = LaunchSettings.GetDefault();
            LaunchSettings.Create(settings, settingsPath);
        }
        
    }
}
