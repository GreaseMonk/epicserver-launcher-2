﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Launcher
{
    public partial class UpdateWriter : Form
    {
        private MainWindow mainWindow;

        public UpdateWriter(MainWindow creator)
        {
            InitializeComponent();
            mainWindow = creator;

            versionTxtBox.Text = Application.ProductVersion.Split('.')[0];
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Generate the JSON file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void generateJsonBtn_Click(object sender, EventArgs e)
        {
            mainWindow.AddConsoleLine("(UpdateWriter) Creating master version file...");

            if (versionTxtBox.Text == String.Empty || downloadURLTxtBox.Text == String.Empty)
            {
                mainWindow.AddConsoleLine("(UpdateWriter) Don't leave any TextBox blank please.");
                return;
            }

            EpicVersionData versionData = new EpicVersionData();

            // ToInt32 can throw FormatException or OverflowException. 
            try
            {
                versionData.version = Convert.ToInt32(versionTxtBox.Text);
            }
            catch (FormatException ex)
            {
                mainWindow.AddConsoleLine("(UpdateWriter) " + "Version input string is not a sequence of digits. > " + ex.Message);
            }
            catch (OverflowException ex)
            {
                mainWindow.AddConsoleLine("(UpdateWriter) " + "The version number cannot fit in an Int32. > " + ex.Message);
            }

            versionData.downloadURL = downloadURLTxtBox.Text;
            versionData.resetSettings = deleteSettingsCheckbox.Checked;

            EpicVersionData.Create(versionData);
            mainWindow.AddConsoleLine("Successfully created master version data file.");

            Close();
        }


    }
}
