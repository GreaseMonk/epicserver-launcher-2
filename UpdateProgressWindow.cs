﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;

namespace Launcher
{
    public partial class UpdateProgressWindow : Form
    {
        private const string titlePrefix = "Downloading version ";
        private EpicVersionData newVersionData;
        private MainWindow mainWindow;

        private string targetPath;

        public UpdateProgressWindow(MainWindow creator, EpicVersionData newVersionData)
        {
            InitializeComponent();
            this.newVersionData = newVersionData;
            mainWindow = creator;
            StartDownload();
            Show();

            if (newVersionData.resetSettings)
                mainWindow.GenerateNewSettingsFile();
        }

        private void StartDownload()
        {
            string targetPath = Directory.GetCurrentDirectory() + @"\v" + newVersionData.version.ToString() + ".zip";
            WebClient webClient = new WebClient();
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
            webClient.DownloadFileAsync(new Uri(newVersionData.downloadURL), targetPath);
            this.targetPath = targetPath;
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            title.Text = e.ProgressPercentage.ToString() + "% - " + titlePrefix + newVersionData.version.ToString();
            progressBar.Value = e.ProgressPercentage;
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            
            mainWindow.AddConsoleLine("(Updater) Download Completed.");
            title.Text = "Download Complete. Unpacking...";
            Hide();
            StartUnpacking();
        }

        private void StartUnpacking()
        {
            mainWindow.AddConsoleLine("(Updater) Starting to unpack...");

            try
            {
                FastZip fz = new FastZip();
                fz.ExtractZip(targetPath, Application.StartupPath, String.Empty);

                File.Delete(targetPath);
            }
            catch (System.Exception ex)
            {
                mainWindow.AddConsoleLine("(Updater) Failed to unpack: " + ex.Message);
                return;
            }

            mainWindow.AddConsoleLine("(Updater) Unpack successful.");

            PrepareNewLauncher();
        }

        private void PrepareNewLauncher()
        {
            string[] newLauncher = Directory.GetFiles(Application.StartupPath, "*_new.exe");
            if (newLauncher.Length > 0)
            {
                Process.Start(newLauncher[0]);

                Process.Start("cmd.exe", "/C choice /C Y /N /D Y /T 3 & Del " + Application.ExecutablePath);

                try
                {
                    Environment.Exit(-1);
                }
                catch (System.Security.SecurityException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

    }
}
