﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Launcher
{
    public partial class LoginWindow : Form
    {
        MainWindow mainWindow = null;
        LaunchInfo launchInfo;

        bool autoStart = false;

        public LoginWindow(MainWindow caller, LaunchInfo info, bool autoStart = true)
        {
            InitializeComponent();

            if(caller != null)
                mainWindow = caller;
            else
                throw new NullReferenceException("MainWindow is undefined");

            this.autoStart = autoStart;
            launchInfo = info;
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void passwordBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                // Write the current filled in username and password
                // to the LaunchInfo and return it to the main window.
                launchInfo.username = usernameBox.Text;
                launchInfo.password = passwordBox.Text;

                mainWindow.CompleteLogin(launchInfo, autoStart);

                Close();
            }

            
        }

        
    }
}
