﻿namespace Launcher
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.Tabs = new System.Windows.Forms.TabControl();
            this.consoleTab = new System.Windows.Forms.TabPage();
            this.consoleBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.modsTab = new System.Windows.Forms.TabPage();
            this.addonsListBox = new System.Windows.Forms.CheckedListBox();
            this.optionsTab = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.launchWithForgeCheckbox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkForUpdatesBtn = new System.Windows.Forms.Button();
            this.versionLabel = new System.Windows.Forms.Label();
            this.autoCheckUpdatesCheckbox = new System.Windows.Forms.CheckBox();
            this.closeWhenMCStartsCheckbox = new System.Windows.Forms.CheckBox();
            this.changelogTab = new System.Windows.Forms.TabPage();
            this.changelogTxtBox = new System.Windows.Forms.TextBox();
            this.startBtn = new System.Windows.Forms.Button();
            this.quickstartBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.minimizeBtn = new System.Windows.Forms.Button();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.usernameBtn = new System.Windows.Forms.Button();
            this.Tabs.SuspendLayout();
            this.consoleTab.SuspendLayout();
            this.modsTab.SuspendLayout();
            this.optionsTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.changelogTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.Tabs.Controls.Add(this.consoleTab);
            this.Tabs.Controls.Add(this.modsTab);
            this.Tabs.Controls.Add(this.optionsTab);
            this.Tabs.Controls.Add(this.changelogTab);
            this.Tabs.ItemSize = new System.Drawing.Size(195, 24);
            this.Tabs.Location = new System.Drawing.Point(12, 81);
            this.Tabs.Name = "Tabs";
            this.Tabs.Padding = new System.Drawing.Point(6, 6);
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(822, 360);
            this.Tabs.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.Tabs.TabIndex = 0;
            this.Tabs.TabStop = false;
            // 
            // consoleTab
            // 
            this.consoleTab.BackColor = System.Drawing.Color.White;
            this.consoleTab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.consoleTab.Controls.Add(this.consoleBox);
            this.consoleTab.Controls.Add(this.textBox1);
            this.consoleTab.Location = new System.Drawing.Point(4, 28);
            this.consoleTab.Name = "consoleTab";
            this.consoleTab.Padding = new System.Windows.Forms.Padding(3);
            this.consoleTab.Size = new System.Drawing.Size(814, 328);
            this.consoleTab.TabIndex = 0;
            this.consoleTab.Text = "Console            ";
            // 
            // consoleBox
            // 
            this.consoleBox.BackColor = System.Drawing.Color.White;
            this.consoleBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.consoleBox.Location = new System.Drawing.Point(7, 7);
            this.consoleBox.Multiline = true;
            this.consoleBox.Name = "consoleBox";
            this.consoleBox.ReadOnly = true;
            this.consoleBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.consoleBox.Size = new System.Drawing.Size(799, 278);
            this.consoleBox.TabIndex = 0;
            this.consoleBox.TabStop = false;
            this.consoleBox.Text = "This is the console";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 300);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(800, 20);
            this.textBox1.TabIndex = 1;
            // 
            // modsTab
            // 
            this.modsTab.BackColor = System.Drawing.SystemColors.Control;
            this.modsTab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modsTab.Controls.Add(this.addonsListBox);
            this.modsTab.Location = new System.Drawing.Point(4, 28);
            this.modsTab.Name = "modsTab";
            this.modsTab.Padding = new System.Windows.Forms.Padding(3);
            this.modsTab.Size = new System.Drawing.Size(814, 328);
            this.modsTab.TabIndex = 1;
            this.modsTab.Text = "Mods            ";
            // 
            // addonsListBox
            // 
            this.addonsListBox.FormattingEnabled = true;
            this.addonsListBox.Location = new System.Drawing.Point(7, 7);
            this.addonsListBox.Name = "addonsListBox";
            this.addonsListBox.Size = new System.Drawing.Size(247, 304);
            this.addonsListBox.TabIndex = 0;
            // 
            // optionsTab
            // 
            this.optionsTab.BackColor = System.Drawing.SystemColors.Control;
            this.optionsTab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.optionsTab.Controls.Add(this.groupBox2);
            this.optionsTab.Controls.Add(this.groupBox1);
            this.optionsTab.Location = new System.Drawing.Point(4, 28);
            this.optionsTab.Name = "optionsTab";
            this.optionsTab.Padding = new System.Windows.Forms.Padding(3);
            this.optionsTab.Size = new System.Drawing.Size(814, 328);
            this.optionsTab.TabIndex = 2;
            this.optionsTab.Text = "Options            ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.launchWithForgeCheckbox);
            this.groupBox2.Location = new System.Drawing.Point(6, 153);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(800, 167);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Minecraft";
            // 
            // launchWithForgeCheckbox
            // 
            this.launchWithForgeCheckbox.AutoSize = true;
            this.launchWithForgeCheckbox.Location = new System.Drawing.Point(6, 19);
            this.launchWithForgeCheckbox.Name = "launchWithForgeCheckbox";
            this.launchWithForgeCheckbox.Size = new System.Drawing.Size(111, 17);
            this.launchWithForgeCheckbox.TabIndex = 2;
            this.launchWithForgeCheckbox.Text = "Launch with forge";
            this.launchWithForgeCheckbox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkForUpdatesBtn);
            this.groupBox1.Controls.Add(this.versionLabel);
            this.groupBox1.Controls.Add(this.autoCheckUpdatesCheckbox);
            this.groupBox1.Controls.Add(this.closeWhenMCStartsCheckbox);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(800, 141);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Launcher";
            // 
            // checkForUpdatesBtn
            // 
            this.checkForUpdatesBtn.Location = new System.Drawing.Point(577, 112);
            this.checkForUpdatesBtn.Name = "checkForUpdatesBtn";
            this.checkForUpdatesBtn.Size = new System.Drawing.Size(217, 23);
            this.checkForUpdatesBtn.TabIndex = 4;
            this.checkForUpdatesBtn.Text = "Check for updates now";
            this.checkForUpdatesBtn.UseVisualStyleBackColor = true;
            this.checkForUpdatesBtn.Click += new System.EventHandler(this.checkForUpdatesBtn_Click);
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Location = new System.Drawing.Point(7, 20);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(51, 13);
            this.versionLabel.TabIndex = 3;
            this.versionLabel.Text = "Version 1";
            // 
            // autoCheckUpdatesCheckbox
            // 
            this.autoCheckUpdatesCheckbox.AutoSize = true;
            this.autoCheckUpdatesCheckbox.Location = new System.Drawing.Point(6, 95);
            this.autoCheckUpdatesCheckbox.Name = "autoCheckUpdatesCheckbox";
            this.autoCheckUpdatesCheckbox.Size = new System.Drawing.Size(163, 17);
            this.autoCheckUpdatesCheckbox.TabIndex = 1;
            this.autoCheckUpdatesCheckbox.Text = "Check for updates on startup";
            this.autoCheckUpdatesCheckbox.UseVisualStyleBackColor = true;
            this.autoCheckUpdatesCheckbox.CheckedChanged += new System.EventHandler(this.autoCheckUpdatesCheckbox_CheckedChanged);
            // 
            // closeWhenMCStartsCheckbox
            // 
            this.closeWhenMCStartsCheckbox.AutoSize = true;
            this.closeWhenMCStartsCheckbox.Location = new System.Drawing.Point(6, 118);
            this.closeWhenMCStartsCheckbox.Name = "closeWhenMCStartsCheckbox";
            this.closeWhenMCStartsCheckbox.Size = new System.Drawing.Size(217, 17);
            this.closeWhenMCStartsCheckbox.TabIndex = 0;
            this.closeWhenMCStartsCheckbox.Text = "Close the launcher when minecraft starts";
            this.closeWhenMCStartsCheckbox.UseVisualStyleBackColor = true;
            this.closeWhenMCStartsCheckbox.CheckedChanged += new System.EventHandler(this.closeWhenMCStartsCheckbox_CheckedChanged);
            // 
            // changelogTab
            // 
            this.changelogTab.Controls.Add(this.changelogTxtBox);
            this.changelogTab.Location = new System.Drawing.Point(4, 28);
            this.changelogTab.Name = "changelogTab";
            this.changelogTab.Padding = new System.Windows.Forms.Padding(3);
            this.changelogTab.Size = new System.Drawing.Size(814, 328);
            this.changelogTab.TabIndex = 3;
            this.changelogTab.Text = "Changelog";
            this.changelogTab.UseVisualStyleBackColor = true;
            // 
            // changelogTxtBox
            // 
            this.changelogTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.changelogTxtBox.Location = new System.Drawing.Point(6, 6);
            this.changelogTxtBox.Multiline = true;
            this.changelogTxtBox.Name = "changelogTxtBox";
            this.changelogTxtBox.ReadOnly = true;
            this.changelogTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.changelogTxtBox.Size = new System.Drawing.Size(802, 342);
            this.changelogTxtBox.TabIndex = 0;
            this.changelogTxtBox.Text = resources.GetString("changelogTxtBox.Text");
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(838, 81);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(145, 45);
            this.startBtn.TabIndex = 1;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // quickstartBtn
            // 
            this.quickstartBtn.Location = new System.Drawing.Point(838, 132);
            this.quickstartBtn.Name = "quickstartBtn";
            this.quickstartBtn.Size = new System.Drawing.Size(145, 46);
            this.quickstartBtn.TabIndex = 2;
            this.quickstartBtn.Text = "Quickstart";
            this.quickstartBtn.UseVisualStyleBackColor = true;
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(938, 15);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(45, 23);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Text = "X";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // minimizeBtn
            // 
            this.minimizeBtn.Location = new System.Drawing.Point(887, 15);
            this.minimizeBtn.Name = "minimizeBtn";
            this.minimizeBtn.Size = new System.Drawing.Size(45, 23);
            this.minimizeBtn.TabIndex = 6;
            this.minimizeBtn.Text = "_";
            this.minimizeBtn.UseVisualStyleBackColor = true;
            this.minimizeBtn.Click += new System.EventHandler(this.minimizeBtn_Click);
            // 
            // passwordLabel
            // 
            this.passwordLabel.Location = new System.Drawing.Point(525, 41);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(356, 23);
            this.passwordLabel.TabIndex = 8;
            this.passwordLabel.Text = "password";
            this.passwordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.passwordLabel.Visible = false;
            // 
            // usernameBtn
            // 
            this.usernameBtn.BackColor = System.Drawing.SystemColors.Control;
            this.usernameBtn.Location = new System.Drawing.Point(525, 15);
            this.usernameBtn.Name = "usernameBtn";
            this.usernameBtn.Size = new System.Drawing.Size(356, 23);
            this.usernameBtn.TabIndex = 9;
            this.usernameBtn.TabStop = false;
            this.usernameBtn.Text = "user@used.com";
            this.usernameBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.usernameBtn.UseVisualStyleBackColor = true;
            this.usernameBtn.Click += new System.EventHandler(this.usernameBtn_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 450);
            this.Controls.Add(this.usernameBtn);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.minimizeBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.quickstartBtn);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.Tabs);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EpicServer Launcher";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainWindow_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainWindow_KeyPress);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainWindow_MouseDown);
            this.Tabs.ResumeLayout(false);
            this.consoleTab.ResumeLayout(false);
            this.consoleTab.PerformLayout();
            this.modsTab.ResumeLayout(false);
            this.optionsTab.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.changelogTab.ResumeLayout(false);
            this.changelogTab.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage consoleTab;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabPage modsTab;
        private System.Windows.Forms.TabPage optionsTab;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Button quickstartBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button minimizeBtn;
        private System.Windows.Forms.TextBox consoleBox;
        private System.Windows.Forms.CheckBox closeWhenMCStartsCheckbox;
        private System.Windows.Forms.CheckBox autoCheckUpdatesCheckbox;
        private System.Windows.Forms.CheckBox launchWithForgeCheckbox;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Button usernameBtn;
        public System.Windows.Forms.CheckedListBox addonsListBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button checkForUpdatesBtn;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.TabPage changelogTab;
        private System.Windows.Forms.TextBox changelogTxtBox;
    }
}

